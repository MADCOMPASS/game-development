# CMake generated Testfile for 
# Source directory: D:/Game Dev5/Game Dev4/Game Dev3/bullet3-2.87/Extras
# Build directory: D:/Game Dev5/Game Dev4/Game Dev3/Extras
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("InverseDynamics")
subdirs("BulletRobotics")
subdirs("obj2sdf")
subdirs("Serialize")
subdirs("ConvexDecomposition")
subdirs("HACD")
subdirs("GIMPACTUtils")
