#include "SoundFMOD.h"
using namespace std;


 SoundFMOD::SoundFMOD() {

	 result = FMOD::System_Create(&system); // Create the main system object.

	 if (result != FMOD_OK) {
		 printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
		 exit(-1);
	 }

	 result = system->init(512, FMOD_INIT_NORMAL, 0);    // Initialize FMOD.

	 if (result != FMOD_OK) {
		 printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
		 exit(-1);
	 }

 }

 SoundFMOD::~SoundFMOD() {
	
 }

 // the theme song
 void SoundFMOD::playThemeSound() {
	result = system->createSound("../FMOD/theme.mp3", FMOD_3D, 0, &thomas);
	if (result != FMOD_OK)
	{
		std::cout << "You have some problems with creating a sound";
	}
	system->playSound(thomas, NULL, false, &channel);
}

 // the sound loop that takes a event from the EventQueue in the main game loop
 void SoundFMOD::soundLoop(Input in) {
	 if (in == N) {
		 result = system->createSound("../FMOD/spawn.mp3", FMOD_3D, 0, &cubeGeneration);
		 if (result != FMOD_OK)
		 {
			 std::cout << "You have some problems with creating a sound";
		 }
		 system->playSound(cubeGeneration, NULL, false, &channel);
	 }
 }
