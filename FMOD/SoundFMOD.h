#pragma once
#include <stdio.h>
#include <iostream>
#include "fmod.hpp"
#include "fmod_errors.h"
#include "Controller.h"
using namespace std;

// dont forget to create a destructor to delete all your sound pointers

class SoundFMOD {

public:

	SoundFMOD();
	~SoundFMOD();
	void playThemeSound();
	void soundLoop(Input in);

	FMOD_RESULT result;
	FMOD::System *system;
	FMOD::Sound *thomas;
	FMOD::Sound *cubeGeneration;
	FMOD::Channel *channel = NULL;

};