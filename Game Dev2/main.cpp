#include <stdio.h>
#include <iostream>
#include "window.h"
#include "Renderer.h"
#include "RenderObject.h"
#include <vector>
#include "Graphics.h"
#include "Controller.h"
#include "SoundFMOD.h"
#include "Physics.h"
#include "src\btBulletDynamicsCommon.h"
#include "Profiling.h"
#include "FileInputOutput.h"

using namespace std;

Window w = Window(1200, 1000);
Renderer r(w);

Graphics graphics(r);
Controller controller;
SoundFMOD soundfmod;
Physics physics;
Profiling profiling;
FileInputOutput fileio;

std::vector<Input> EventQueue;

Input temp = GAMEOVER;
bool gameOVER = false;
int main() {

	soundfmod.playThemeSound(); // aka the theme song
	// fileinputoutput
	vector<float> fileinput = fileio.readFile();
	physics.setConfig(fileinput);

	while (w.UpdateWindow()) {

		float msec = w.GetTimer()->GetTimedMS();

		// input devices
		Input controllerInput = controller.ControllerLoop();
		if (controllerInput == ESC) {
			break;
		} 
		if (controllerInput == R) {
			gameOVER = false;
			EventQueue.clear();
		}
		if (gameOVER == false) {
			EventQueue.push_back(controllerInput);
		}
		profiling.subSystemInformation.push_back(w.GetTimer()->GetTimedMS());

		// physics
		vector<btTransform> list = physics.PhysicsLoop(EventQueue[0]); 
		if (list.size() == 0) { // for if the physics system wants to inform other sub systems that the game is over by setting the size of list to 0
			if (EventQueue[0] != ESC) {
				EventQueue[0] = temp;
			}
			gameOVER = true; 
		}
		profiling.subSystemInformation.push_back(w.GetTimer()->GetTimedMS());

		// graphics
		graphics.display(msec, w, r, list, controllerInput);
		profiling.subSystemInformation.push_back(w.GetTimer()->GetTimedMS());
		
		// Sound
		soundfmod.soundLoop(EventQueue[0]);
		profiling.subSystemInformation.push_back(w.GetTimer()->GetTimedMS());
		
		// profiling
		profiling.displayProfiling(EventQueue[0]);

		// Clearing EventQueue
		if (!EventQueue.empty() && gameOVER == false) {
			EventQueue.erase(EventQueue.begin(), EventQueue.begin() + 1);// Remove the first event and resize the EventQueue
		}

	}

}