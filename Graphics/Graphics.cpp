#include "Graphics.h"	


Graphics::Graphics(Renderer &r) {

	o.SetMesh(m);
	o.SetShader(texture);
	o.SetType(0);

	// background
	backImage.SetMesh(m);
	backImage.SetShader(texture);
	backImage.SetType(2);
	backImage.SetModelMatrix(Matrix4::Translation(Vector3(0, 20, -50)) * Matrix4::Scale(Vector3(50, 50, 0)));

	r.AddRenderObject(o);
	r.AddRenderObject(backImage);

	r.SetProjectionMatrix(Matrix4::Perspective(1, 100, 1.33f, 45.0f));

	r.SetViewMatrix(Matrix4::BuildViewMatrix(Vector3(0, 0,0), Vector3(0, 0, -10)));
	r.SetViewMatrix(Matrix4::Translation(Vector3(0, -2, -20.0f)));
}

Graphics::~Graphics() {

}

int sizeoflist = 1; // default list of objects that exists in the constructor
bool displayGameover = false;
int score; // the score equals the object list -1, but i must remove the generate object per n, and have it for per collision with object and object -1

float cameraX = 0;
float cameraY = -2;
float highestY = 0;

float currentY = -2; // smooth transition of camera when pressing N
float maxY = -2;

void Graphics::display(float msec, Window &w, Renderer &r, vector<btTransform> list, Input in) {
	
	// if the matrix list is empty, then the game is over
	if (list.size() == 0) {

	// if I havent display gameover, then display game over
		if (!displayGameover) { 

			cout << "GAMEOVER" << endl;
			cout << "Your Score is: " << r.renderObjects.size() - 2 << endl;
			displayGameover = true;

			// finding the higest point of the tower
			for (int i = 2; i < r.renderObjects.size(); i++) {
				int g = r.renderObjects[i]->GetWorldTransform().GetPositionVector().y;
				g = g - (g + g);
				if (g < highestY) {
					highestY = g;
				}
			}

			r.SetViewMatrix(Matrix4::Translation(Vector3(0, cameraY, -20.0f) + r.renderObjects[2]->GetWorldTransform().GetPositionVector()));
		}

		// moving the camera until it reaches highest point
		if (cameraY > highestY) {
			r.SetViewMatrix(Matrix4::Translation(Vector3(0, cameraY -= 0.01, -20.0f) + r.renderObjects[2]->GetWorldTransform().GetPositionVector()));
		}

		r.UpdateScene(msec);
		r.ClearBuffers();
		r.RenderScene();
		r.SwapBuffers();
		return;
	}

	// restarting the game
	if (in == R) {
		r.renderObjects.erase(r.renderObjects.begin() + 2, r.renderObjects.begin() + r.renderObjects.size());
		r.SetViewMatrix(Matrix4::Translation(Vector3(0, -2, -20.0f)));
		displayGameover = false;
		cameraY = -2;
		highestY = 0;
		currentY = -2;
		maxY = -2;
	}

	float m1[16];

	// if a new matrix is return in the list from physics, create a new renderobject
	if (list.size() > sizeoflist) {
		for (int i = 0; i < list.size() - sizeoflist; i++) {
			RenderObject* o = new RenderObject(m, texture);
			o->SetType(1);
			r.renderObjects.push_back(o);
		}
		maxY -= 1.8;
	}

	sizeoflist = list.size();

	// setting the ground matrix
	list[0].getOpenGLMatrix(m1);
	m1[0] = 100;
	m1[5] = 1.1;
	m1[10] = 100;
	r.renderObjects[0]->SetModelMatrix(m1);

	if (list.size() > 0) { // meaning the physics class has added an additional object
		for (int i = 1; i < list.size(); i++) {
			
			if (i > 1 && currentY > maxY) { // to ensure the camera doesnt move on the first drop
				r.SetViewMatrix(Matrix4::Translation(Vector3(0, currentY-= 0.01, -20.0f)));
			}
			//ignore the background object
			list.at(i).getOpenGLMatrix(m1);

			r.renderObjects[i + 1]->SetModelMatrix(m1);

		
		}
	}

	r.UpdateScene(msec);
	r.ClearBuffers();
	r.RenderScene();
	r.SwapBuffers();
}

