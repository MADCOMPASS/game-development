#pragma once

#include <stdio.h>
#include <iostream>
#include "window.h"
#include "Renderer.h"
#include "RenderObject.h"
#include <vector>
//#include "Physics.h"
#include "btBulletCollisionCommon.h"
#include "Controller.h"

using namespace std;

class Graphics {

public:

	Graphics(Renderer &r);
	~Graphics();

	void display(float msec, Window &w, Renderer &r, vector<btTransform> list, Input in); //should have Quaternion cube2 so the cube can bounce

	// initial objects at start up
	RenderObject o;
	RenderObject o2;
	RenderObject o3;
	RenderObject backImage;
	
	Mesh*	m = Mesh::LoadMeshFile("cube.asciimesh");

	Shader* s = new Shader("basicvert.glsl", "basicFrag.glsl");
	Shader* texture = new Shader("groundVert.glsl", "groundFrag.glsl");

	
};