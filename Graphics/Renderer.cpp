#include "Renderer.h"

Renderer::Renderer(Window &parent) : OGLRenderer(parent)	{

	 glEnable(GL_DEPTH_TEST);
	 glActiveTexture(GL_TEXTURE0);
	 ground = SOIL_load_OGL_texture("ground.jpg", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS);
	 glActiveTexture(GL_TEXTURE1);
	 background = SOIL_load_OGL_texture("background.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS);
	 glActiveTexture(GL_TEXTURE2);
	 block = SOIL_load_OGL_texture("block.jpg", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS);



}

Renderer::~Renderer(void)	{
	 
}

void	Renderer::RenderScene() {

	if (Keyboard::KeyDown(KEY_V))
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	for(vector<RenderObject*>::iterator i = renderObjects.begin(); i != renderObjects.end(); ++i ) {
		Render(*(*i));
	}
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void	Renderer::Render(const RenderObject &o) {
	modelMatrix = o.GetWorldTransform();

	if(o.GetShader() && o.GetMesh() && o.GetType() == 1) {
		GLuint program = o.GetShader()->GetShaderProgram();
		
		glUseProgram(program);

		UpdateShaderMatrices(program);

		glUniform1i(glGetUniformLocation(program, "tex2"), 2);

		o.Draw();
	}

	if (o.GetShader() && o.GetMesh() && o.GetType() == 0) {
		GLuint program = o.GetShader()->GetShaderProgram();

		glUseProgram(program);

		UpdateShaderMatrices(program);

		glUniform1i(glGetUniformLocation(program, "tex2"), 0);

		o.Draw();
	}

	if (o.GetShader() && o.GetMesh() && o.GetType() == 2) {
		GLuint program = o.GetShader()->GetShaderProgram();

		glUseProgram(program);

		UpdateShaderMatrices(program);

		glUniform1i(glGetUniformLocation(program, "tex2"),1);

		o.Draw();
	}

	for(vector<RenderObject*>::const_iterator i = o.GetChildren().begin(); i != o.GetChildren().end(); ++i ) {
		Render(*(*i));
	}

}

void	Renderer::UpdateScene(float msec) {
	for(vector<RenderObject*>::iterator i = renderObjects.begin(); i != renderObjects.end(); ++i ) {
		(*i)->Update(msec);
	}
}

GLuint Renderer::LoadTexture(string filename)
{

	GLuint tex = SOIL_load_OGL_texture(filename.c_str(), SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS);
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBindTexture(GL_TEXTURE_2D, tex);
	return tex;


}
