#include "Physics.h"

Physics::Physics()
{
	dynamicsWorld->setGravity(btVector3(0, -10, 0));
	//----------------- objects ------------------//

	//-------shapes------//
	// dynamic rigid body/ground
	groundShape = new btStaticPlaneShape(btVector3(0, 1, 0), 1);
	fallShape = new btBoxShape(btVector3(1, 1, 1)); // cube that takes halve extent value, meaning its 1,1,1

   //-------type of body----///
   //rigid body for ground
	btDefaultMotionState* groundMotionState = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, -1, 0)));

	//------physics of the body-----//
	btRigidBody::btRigidBodyConstructionInfo groundRigidBodyCI(0, groundMotionState, groundShape, btVector3(0, 0, 0));
	groundRigidBody = new btRigidBody(groundRigidBodyCI);

	btTransform temp;
	list.push_back(temp);
	
	ObjectList.push_back(groundRigidBody);

	dynamicsWorld->addRigidBody(groundRigidBody);

}


Physics::~Physics()
{
	
	delete dynamicsWorld;

	delete solver;

	delete overlappingPairCache;

	delete dispatcher;

	delete collisionConfiguration;

	delete groundShape;

	delete fallShape;

	delete fallMotionState;

	delete fallRigidBody;

	delete fallRigidBody2;

	delete groundRigidBody;

}

bool gameOver = false;
bool collisionPrevious = false;
btTransform groundMatrix;

std::vector<btTransform> Physics::PhysicsLoop(int command)
{
	if (command == GAMEOVER && creativeMode != true) {
		list.clear();
		return list;
	}

	// handeling call back for gameover collision
	struct	MyContactResultCallback : public btCollisionWorld::ContactResultCallback
	{
		virtual	btScalar addSingleResult(btManifoldPoint& cp, const btCollisionObjectWrapper* colObj0Wrap, int partId0, int index0, const btCollisionObjectWrapper* colObj1Wrap, int partId1, int index1)
		{
			gameOver = true;
			return 1.0f;
		}
	};

	// handeling call back for collison with previous collision
	struct	MyContactResultCallbackPrevious : public btCollisionWorld::ContactResultCallback
	{
		virtual	btScalar	addSingleResult(btManifoldPoint& cp, const btCollisionObjectWrapper* colObj0Wrap, int partId0, int index0, const btCollisionObjectWrapper* colObj1Wrap, int partId1, int index1)
		{
			collisionPrevious = true;
			return 1.0f;
		}
	};

	// collisionDetection with any object and the ground
	MyContactResultCallback result;
	if (ObjectList.size() > 2) {
		for (int i = 2; i < ObjectList.size(); i++) {
			dynamicsWorld->contactPairTest(ObjectList[0], ObjectList[i], result);
		}
	}

	// collisionDetection wth the newest object and the previous
	MyContactResultCallbackPrevious result2;
	if (ObjectList.size() > 2) {
		dynamicsWorld->contactPairTest(ObjectList[ObjectList.size() - 1], ObjectList[ObjectList.size() - 2], result2);
	}

	// creating a new object and only if the n-1 is in contact with n-2
	if (command == N && ObjectList.size() <= 2) {
		newObject();
	} else if (command == N && ObjectList.size() > 2 && collisionPrevious && creativeMode == false) {
		newObject();
	} else if (command == N){
		newObject();
	}

	// Controlling the latest block, and can turn these into switch statments
	if (command == W && collisionPrevious != true) { // and collision with lateast cube in objectlist = false

		ObjectList[ObjectList.size() - 1]->applyCentralImpulse(btVector3(0, 0, -0.1));
		ObjectList[ObjectList.size() - 1]->activate(true);

	}

	if (command == A && collisionPrevious != true) { // and collision with lateast cube in objectlist = false
				
		ObjectList[ObjectList.size() - 1]->applyCentralImpulse(btVector3(-0.1, 0, 0));
		ObjectList[ObjectList.size() - 1]->activate(true);
	
	}

	if (command == S && collisionPrevious != true) { // and collision with lateast cube in objectlist = false

		ObjectList[ObjectList.size() - 1]->applyCentralImpulse(btVector3(0, 0, 0.1));
		ObjectList[ObjectList.size() - 1]->activate(true);

	}

	if (command == D && collisionPrevious != true) { // and collision with lateast cube in objectlist = false

		ObjectList[ObjectList.size() - 1]->applyCentralImpulse(btVector3(0.1, 0, 0));
		ObjectList[ObjectList.size() - 1]->activate(true);

	}

	// restarting the game
	if (command == R) {

		if (gameOver) { // restart for if the game is already over
			list.push_back(groundMatrix);
			gameOver = false;
		}
		else { // restarting the game in motion
			list.erase(list.begin() + 1, list.begin() + list.size());
		}

		ObjectList.erase(ObjectList.begin() + 1, ObjectList.begin() + ObjectList.size());
		
		for (int i = dynamicsWorld->getNumCollisionObjects() - 1; i > 0; i--) {// > and not >= 0 so it doesnt delete the groud
			btCollisionObject* obj = dynamicsWorld->getCollisionObjectArray()[i];
			dynamicsWorld->removeCollisionObject(obj);
			delete obj;
		}
	}

	collisionPrevious = false;

	dynamicsWorld->stepSimulation(1.0f / 60.f + (ObjectList.size() * speedIncreasePerBlock), 10);

	for (int i = 0; i < ObjectList.size(); i++) {
		
		list[i] = ObjectList[i]->getWorldTransform();
		
	}

	// emptying the list if the gameover
	if (gameOver && creativeMode != true) {
		list.clear();
	} else  {
		groundMatrix = list[0];
	}
	return list;
}

void Physics::newObject()
{
	btScalar mass = 1;
	btVector3 fallInertia(0, 0, 0);
	fallShape->calculateLocalInertia(mass, fallInertia);
	btDefaultMotionState* fallMotionStateNew;

	if (!hardMode) {
		if (ObjectList.size() > 1) {
			fallMotionStateNew = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(((float(rand()) / float(RAND_MAX)) * (maxSpawnRange - minSpawnRange)) + minSpawnRange, spawnDistance * ObjectList.size(), 0)));
		}
		else {
			fallMotionStateNew = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, spawnDistance * ObjectList.size(), 0)));
		}
		// hardmode, randomise the z coordniate
	} else {
		if (ObjectList.size() > 1) {
			fallMotionStateNew = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(((float(rand()) / float(RAND_MAX)) * (maxSpawnRange - minSpawnRange)) + minSpawnRange, spawnDistance * ObjectList.size(), ((float(rand()) / float(RAND_MAX)) * (2 - -2)) + -2)));
		}
		else {
			fallMotionStateNew = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, spawnDistance * ObjectList.size(), ((float(rand()) / float(RAND_MAX)) * (2 - -2)) + -2)));
		}
	}

	btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(mass, fallMotionStateNew, fallShape, fallInertia);

	btRigidBody* newCube = new btRigidBody(fallRigidBodyCI);

	ObjectList.push_back(newCube);
	dynamicsWorld->addRigidBody(newCube);

	btTransform* cube = new btTransform();
	list.push_back(*cube);
}

void Physics::creative() {

	for (int i = 7; i < coord.size() - 1; i += 3) {

		btScalar mass = 1;
		btVector3 fallInertia(0, 0, 0);
		fallShape->calculateLocalInertia(mass, fallInertia);
		btDefaultMotionState* fallMotionStateNew = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(coord[i], coord[i+1], coord[i+2])));;

		btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(mass, fallMotionStateNew, fallShape, fallInertia);

		btRigidBody* newCube = new btRigidBody(fallRigidBodyCI);

		ObjectList.push_back(newCube);
		dynamicsWorld->addRigidBody(newCube);

		btTransform* cube = new btTransform();
		list.push_back(*cube);

	}

}

void Physics::setConfig(vector<float> config)
{
	coord = config;
	dynamicsWorld->setGravity(btVector3(0, config[0], 0));
	maxSpawnRange = config[1];
	minSpawnRange = config[2];
	spawnDistance = config[3];
	speedIncreasePerBlock = (config[4] / 1000);
	
	if (config[5] == 1) {
		hardMode = true;
	}

	if (config[6] == 1) {
		creativeMode = true;
		creative();
	}
}

