#pragma once
#include <stdio.h>
#include <iostream>
#include <string>
//#include "CommonInterfaces\CommonRigidBodyBase.h"
#include "btBulletDynamicsCommon.h"
#include "examples\CommonInterfaces\CommonRigidBodyBase.h"
#include <vector>
#include "Controller.h"


class Physics {

public:
	Physics();
	~Physics();

	std::vector<btTransform> PhysicsLoop(int command);

	///collision configuration contains default setup for memory, collision setup. Advanced users can create their own configuration.
	btDefaultCollisionConfiguration* collisionConfiguration = new btDefaultCollisionConfiguration();

	///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
	btCollisionDispatcher* dispatcher = new btCollisionDispatcher(collisionConfiguration);

	///btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
	btBroadphaseInterface* overlappingPairCache = new btDbvtBroadphase();

	///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
	btSequentialImpulseConstraintSolver* solver = new btSequentialImpulseConstraintSolver;

	btDiscreteDynamicsWorld* dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfiguration);

	//-------------------- shape of objects ---------------//
	btCollisionShape* groundShape = new btStaticPlaneShape(btVector3(0, 1, 0), 1);

	btCollisionShape* fallShape = new btBoxShape(btVector3(0.5, 0.5, 0.5)); // cube that takes halve extent value, meaning its 1,1,1

	btDefaultMotionState* fallMotionState =
		new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 50, 0)));

	
	void newObject();
	void setConfig(vector<float> config); // set the gravity depending on the file
	void creative();

	btRigidBody* fallRigidBody;
	btRigidBody* fallRigidBody2;
	btRigidBody* groundRigidBody;
	vector<btTransform> list;
	vector<btRigidBody*> ObjectList;
	vector<float>coord;

	float maxSpawnRange = 3;
	float minSpawnRange = -3;
	boolean hardMode = false; // maybe i could include music for the harder difficulty
	float spawnDistance = 3;
	float speedIncreasePerBlock = 2;
	float initialBlocks = 0;
	bool creativeMode = false;
};