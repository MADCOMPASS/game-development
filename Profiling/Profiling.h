#pragma once
#include <stdio.h>
#include <iostream>
#include <vector>
#include "Controller.h"

class Profiling {

public:

	Profiling();
	~Profiling();

	void displayProfiling(Input in);
	bool pro = false;
	std::vector<float> subSystemInformation;
	float totalMsec = 0.0f;
	int fps = 0;

};
