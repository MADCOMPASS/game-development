# CMake generated Testfile for 
# Source directory: D:/Game Dev5/Game Dev4/Game Dev3/bullet3-2.87/test
# Build directory: D:/Game Dev5/Game Dev4/Game Dev3/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("InverseDynamics")
subdirs("SharedMemory")
subdirs("gtest-1.7.0")
subdirs("collision")
subdirs("BulletDynamics")
